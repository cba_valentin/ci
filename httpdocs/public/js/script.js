$(function () {
    $('.navbar-toggle, #search-trigger').click(function () {
        $('.navbar-nav').toggleClass('slide-in');
        $('.side-body').toggleClass('body-slide-in');
        if($('#search').hasClass("collapse"))
        {
            $('#search').removeClass('collapse');
        }else
        {
            $('#search').removeClass('in').addClass('collapse').slideUp(200);    
        }
        

        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').toggleClass('slide-in');
        
    });
   
   // Remove menu for searching
   $("#dropdown").click(function (event){
        event.preventDefault();
        var item = $("#"+event.target.parentNode.children[1].id)
        if(item.hasClass("collapse")){
            item.removeClass("collapse");
        }else
        {
            item.addClass('collapse');
        }
   });

   $("#sign_in").click(function (){
        window.location = "/home/login";
   });

   $("#register").click(function ()
   {
        window.location = "/home/register";
   });

   $("#contact").click(function (event){
        event.preventDefault();
        $.ajax({
            url:"/user/home/contact",
            success: function (data){
                $("#container").html(data);
            },
        });
   });

   $(document).on("click", "#send_contact", function (){
   // $("#send_contact").click(function (){
        if($("#tieude").val().trim() && $("#noidung").val().trim())
        {
            $.ajax({
                type: "POST",
                url:"/home/contact",
                data:{tieude:$("#tieude").val(), noidung: $("#noidung").val()},
                success: function(data){
                    if(data == "ok")
                    {
                        alert("Cảm ơn bạn đã chia sẽ với chúng tôi!!");
                        $("#tieude").val("");
                        $("#noidung").val("");
                    }else{
                        alert("Xin lỗi đã có lỗi phát sinh. Mời bạn thử lại sau!!");
                    }
                },
            })
        }else{
            alert("Bạn cần nhập tiêu đề và nội dung");
        }
   });

   $("#sign_out").click(function (){
        window.location = "/home/logout";
   });

});
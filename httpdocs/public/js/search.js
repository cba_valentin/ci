$(function () {
	'use strict';
	// var countriesArray = $.map(countries, function (value, key) { return { value: value, data: key }; });

	$('#tags').autocomplete({
		serviceUrl: '/truyen/home/getSearchInfo',
        // lookup: countriesArray,
        onSelect: function(suggestion) {
            // $('#selction-ajax').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
            window.location = "/truyen/home/detail/" + suggestion.data;
        },
    });

    $('#searchTruyen').autocomplete({
		serviceUrl: '/truyen/home/getSearchInfo',
        // lookup: countriesArray,
        onSelect: function(suggestion) {
        	$("#truyen_code").val(suggestion.data);
        	// alert("test");
            // $('#selction-ajax').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
            // window.location = "/truyen/home/detail/" + suggestion.data;
        },
    });
});
<?php 
/**
* 
*/
class Chuong extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function insertChuong($data)
	{
		$this->db->where("chuong_code", $data["chuong_code"]);
		$row = $this->db->get("chuong")->row();
		if($row)
		{
			// $this->db->where("chuong_id", $row->chuong_id);
			// $this->db->update("chuong", $data);
			return;
		}
		$this->db->insert("chuong", $data);
	}

	public function getChuongByCodeTruyen($codetruyen)
	{
		$this->db->cache_on();
		$this->db->where("truyen_code", $codetruyen);
		$this->db->order_by("chuong_num", "asc");
		return $this->db->get("chuong")->result();
	}

	public function getChuongMoiNhat($codetruyen, $limit)// 
	{
		$this->db->where("truyen_code", $codetruyen);
		$this->db->order_by("chuong_num", "desc");
		$this->db->limit($limit);
		return $this->db->get("chuong")->result();
	}

	public function getChuongsByCodeChuong($code_chuong) // lấy danh sách các chương còn lại từ code_chuong
	{
		$this->db->cache_on();
		$this->db->where("chuong_code", $code_chuong);
		$chuong = $this->db->get("chuong")->row();

		$this->db->cache_on();
		$this->db->where("truyen_code", $chuong->truyen_code);
		$this->db->order_by("chuong_num", "desc");
		return $this->db->get("chuong")->result();
	}

	public function getChuongByCodeChuong($code_chuong) 
	{
		$this->db->where("chuong_code", $code_chuong);
		$chuong = $this->db->get("chuong")->row();
		return $chuong;
	}

	public function updateChuong()//update so chuong "chuong_num"
	{
		$this->db->select("chuong_id, chuong_code");
		$chuongs = $this->db->get("chuong")->result();

		foreach ($chuongs as $row) {
			$arr = explode("-", $row->chuong_code);

			$row->chuong_num = (is_numeric($arr[count($arr)-2]))?$arr[count($arr)-2].".".$arr[count($arr)-1]:$arr[count($arr)-1];
			$this->db->where("chuong_id", $row->chuong_id);
			unset($row->chuong_id);
			$this->db->update("chuong", $row);
		}
	}
}
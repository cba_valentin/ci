<?php

class Theloai extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }

    public function getAllTheLoai()
    {
    	print_r($this->db->get("theloai"));
    }

    public function insertTheLoai($data)
    {
        $this->db->where("theloai_code", $data["theloai_code"]);
        $row = $this->db->get("theloai")->row();
        if($row)
        {
            return;
        }
    	$this->db->insert("theloai", $data);
    }

    public function getTheLoaiByListCode($codes)
    {
        if(!$codes)
            return array();
        $arr = explode(",", $codes);
        foreach ($arr as $key => $value) {
            if($value != "")
            {
                $this->db->or_where("theloai_code", $value);    
            }
        }
        return $this->db->get("theloai")->result();
    }
}
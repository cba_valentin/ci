<?php

class Truyen extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }

    public function insertTruyen($data)
    {
    	$this->db->insert("truyen", $data);
    }

    public function GetAll()
    {
    	$this->db->where("truyen_ten != ''");
    	$this->db->order_by("truyen_ten", "asc");
    	return $this->db->get("truyen")->result();
    }

    public function updateTruyen($data)
    {
    	$this->db->where("id", $data->id);
    	unset($data->id);
    	$this->db->update("truyen", $data);
    }
    public function updateBatchTruyen($data)
    {
    	// $this->db->where("id", $data->id);
    	// unset($data->id);
    	$this->db->update_batch("truyen", $data, "id");
    }

    public function record_count()
	{
		return $this->db->count_all("truyen");
	}

	public function fetch_truyens($limit, $start)
	{
		$this->db->cache_on();
		$this->db->order_by("truyen_date_update", "desc");
		$this->db->limit($limit, $start);
		$query = $this->db->get("truyen");
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function getTruyenByCode($code)
	{
		$this->db->where("truyen_code", $code);
		$truyen = $this->db->get("truyen")->row();
		if($truyen){
			return $truyen;
		}else{
			$this->db->insert("truyen", array("truyen_code" => $code));
			$this->db->where("truyen_code", $code);
			return $this->db->get("truyen")->row();
		}
	}

	public function getSearchInfo($query)
	{
		$this->db->cache_on();
		$this->db->select("truyen_ten as value, truyen_code as data");
		$this->db->where("truyen_ten like '%".$query."%'");
		return $this->db->get("truyen")->result_array();
	}

}
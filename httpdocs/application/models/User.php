<?php

class User extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}

	function checkUser($user)
	{
		$this->load->library('encrypt');
		$this->db->where("user_name", $user["user_name"]);
		$row = $this->db->get("user")->row();
		if($this->encrypt->decode($row->user_password) == $user["user_password"])
		{
			return $row;
		}
		else{
			return false;
		}
	}

	function getUserByUserName($user_name)
	{
		$this->db->where("user_name", $user_name);
		return $this->db->get("user")->row();
	}

	function insertUser($user)
	{
		$this->db->insert("user", $user);

		return $this->db->insert_id();
	}

	function updateUser($user)
	{
		$this->db->where("user_name", $user["user_name"]);
		unset($user["id"]);
		$this->db->update("user", $user);
	}

}
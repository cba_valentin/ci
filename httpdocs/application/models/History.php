<?php
class History extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}
	
	function insertHistory($data)
	{
		$this->db->where("truyen_code", $data["truyen_code"]);
		$this->db->where("user_id", $data["user_id"]);
		$row = $this->db->get("history")->row();
		if($row)
		{
			$this->db->where("id", $row->id);
			unset($data["id"]);
			$this->db->update("history", $data);
		}else{
			$this->db->insert("history", $data);
		}
	}

	function getHistory()
	{
		if($this->session->user)
		{
			$this->db->where("user_id", $this->session->user->id);
			$this->db->order_by("date_update", "DESC");
			$result = $this->db->get("history")->result();
			return $result;
		}else
		{
			return false;
		}
	}

}
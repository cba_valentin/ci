<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once BASEPATH.'../application/core/USER_Controller.php';

class Home extends USER_Controller {
	function __construct()
    {
    	parent::__construct();
    	$this->load->model("User");
    	$this->load->model("History");
    	$this->load->model("truyen/Truyen");
    	$this->load->model("truyen/Chuong");
    }

    public function index()
    {
    	$this->load->library('form_validation');
		$this->load->library('encrypt');
		$this->config->set_item('language', 'vietnamese');
		$config = array(
				array(
					'field' => 'user[user_display_name]',
					'label' => 'Họ và Tên',
					'rules' => 'trim|required|max_length[20]'
				),
				array(
					'field' => 'user[user_email]',
					'label' => 'Email',
					'rules' => 'trim|required|max_length[50]|valid_email'
				),
				array(
					'field' => 'user[user_name]',
					'label' => 'Tên đăng nhập',
					'rules' => 'trim|required|max_length[20]|min_length[6]'
				),
				array(
					'field' => 'user[user_phone]',
					'label' => 'Số điện thoại',
					'rules' => 'trim|max_length[12]|numeric'
				),
				array(
					'field' => 'user[user_birthday]',
					'label' => 'Ngày sinh',
					'rules' => 'regex_match[/([0-9]+)\/([0-9]+)\/([0-9]+)/]'
				),
				array(
					'field' => 'user[user_password]',
					'label' => 'Mật khẩu',
					'rules' => 'trim|required|min_length[6]'
				),
				array(
					'field' => 'user[user_password_confirm]',
					'label' => 'Xác nhận mật khẩu',
					'rules' => 'trim|required|min_length[6]|matches[user[user_password]]'
				),
			);
		// $this->form_validation->set_message();
		$this->form_validation->set_rules($config);
		if($this->form_validation->run())
		{
			$user = $this->input->post("user");
			// print_r($user)
			if($user['user_password'] != "venhathoi.vn")
			{
				$user['user_password'] = $this->encrypt->encode($user['user_password']);
			}else{
				unset($user['user_password_confirm']);	
			}
			unset($user['user_password_confirm']);
			$this->User->updateUser($user);
			$this->session->user = (object)$user;
		}
    	$this->load->view("user/home.html", array("user" => $this->session->user));
    }

    public function history()
    {
    	$result = $this->History->getHistory();
    	foreach ($result as $item) {
    		$item->chuong = $this->Chuong->getChuongByCodeChuong($item->chuong_code);
    		$item->truyen = $this->Truyen->getTruyenByCode($item->truyen_code);
    	}
    	$this->load->view("user/history.html", array("list_truyen" => $result));
    }

    public function contact()
    {
    	echo @file_get_contents(BASEPATH."../application/views/contact.html");
    }
}

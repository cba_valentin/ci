<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
    	error_reporting(0);
    	parent::__construct();
		$this->load->model('truyen/Truyen');
		$this->load->model('truyen/Theloai');
		$this->load->model('truyen/Chuong');
		$this->load->helper("url");
		$this->load->model("History");
    }

	public function index($page = 0)
	{
		$config               = array();
		$config["base_url"]   = base_url()."truyen/home/index";
		$config["total_rows"] = $this->Truyen->record_count();
		$config["per_page"]   = 30;
		$choice               = $config["total_rows"] / $config["per_page"];
		$config["num_links"]  = 5;

		$this->pagination->initialize($config);
		
		$data["result"] = $this->Truyen->fetch_truyens($config["per_page"], $page);
		$data["link"]   = $this->pagination->create_links();

		$this->load->view('/truyen/home.html', $data);
	}

	public function detail($code = null)
	{
		if(!$code)
		{
			redirect("/truyen");
		}
		$truyen                  = $this->Truyen->getTruyenByCode($code);
		$truyen->truyen_the_loai = $this->Theloai->getTheLoaiByListCode($truyen->truyen_the_loai);
		$data["truyen"]          = $truyen;
		$data["chuongs"]         = $this->Chuong->getChuongByCodeTruyen($truyen->truyen_code);
		$data["chuong_moi"]      = $this->Chuong->getChuongMoiNhat($truyen->truyen_code, 5);
		$this->load->view("/truyen/detail.html", $data);
	}

	public function read($code = null){
		if(!$code)
		{
			echo "<script>history.go(-1)</script>";
		}

		$detail = @file_get_contents('http://truyentranhtuan.com/'.$code);
		preg_match_all("/var slides_page_url_path = \[\"(.*)\"\]/", $detail, $match);
		
		if(isset($match[1][0]))
		{
			$match[1][0] = str_replace("\"", "", $match[1][0]);
			$data["images"]  = explode(",", $match[1][0]);
		}else{
			preg_match_all("/var slides_page_path = \[\"(.*)\"\]/", $detail, $match);
			$match[1][0] = str_replace("\"", "", $match[1][0]);
			$data["images2"]  = explode(",", $match[1][0]);
			// echo "<script>history.go(-1)</script>";
		}

		$data["chuongs"] = $this->Chuong->getChuongsByCodeChuong($code);
		$data["code"] = $code;
		$data["chuong"] = $this->Chuong->getChuongByCodeChuong($code);
		$data["truyen"] = $this->Truyen->getTruyenByCode($data["chuong"]->truyen_code);

		if($this->session->user)
		{
			$item["chuong_code"] = $code;
			$item["truyen_code"] = $data["chuong"]->truyen_code;
			$item["date_update"] = date("Y-m-d H:i:s");
			$item["user_id"] = $this->session->user->id;
			$this->History->insertHistory($item);	
		}

		$this->load->view("/truyen/read.html", $data);	
	}

	public function DanhSachTruyen()
	{
		// echo ord("Z");
		// echo chr("");
		$data["truyens"] = $this->Truyen->GetAll();
		$this->load->view("truyen/list.html", $data);
	}

	public function getSearchInfo()
	{
		$result = $this->Truyen->getSearchInfo($_GET["query"]);
		echo json_encode(array("suggestions" => $result));
		// print_r($result);
	}

	public function getImageTruyen()
	{
		echo base64_encode(@file_get_contents($_POST["url"]));
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once BASEPATH.'../application/core/ADMIN_Controller.php';

class Home extends ADMIN_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model("User");
    }

    public function index()
	{
		$this->load->view('admin/home.html');
	}
	
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once BASEPATH.'../application/core/ADMIN_Controller.php';

class Truyen_api extends ADMIN_Controller {

	function __construct()
    {
        parent::__construct();
        ini_set('display_errors', 'Off');
		ini_set('display_startup_errors', 'Off');
		error_reporting(0);
		libxml_use_internal_errors(true);
		$this->load->model('truyen/Truyen');
		$this->load->model('truyen/Theloai');
		$this->load->model('truyen/Chuong');
		$this->load->model('User');
		$this->load->helper('download');
    }

	public function index()
	{
		$this->load->view('admin/home.html');
	}

	public function getTheLoaiTruyen()
	{
		$homepage = @file_get_contents('http://webtruyen.com/');
		$doc = new DOMDocument();
		$doc->loadHTML($homepage);

		$finder = new DomXPath($doc);
		
		$nodes = $finder->query("//*[contains(@class, 'menu_theloai')]")[0]->childNodes;
		$data = array();
		for($i = 0 ; $i < $nodes->length; $i += 2 )
		{
			$a = ($nodes[$i]->childNodes[0]->attributes[0]->nodeValue);
			$code = str_replace("http://webtruyen.com/","", $a);
			$code = substr($code, 0, strlen($code) - 1);

			$item = array();
			$item["theloai_code"] = $code;
			$item["theloai_ten"]  = $nodes[$i]->textContent;
			$data[$i] = $item;
			$this->Theloai->insertTheLoai($item);
		}
	}

	public function getDanhSachTruyen()
	{
		$homepage = @file_get_contents('http://truyentranhtuan.com/danh-sach-truyen');
		$homepage = str_replace(array("\n", "\r", "\t", "  "), "", $homepage);
		preg_match_all("/<div class=\"manga-focus\"><span class=\"manga\"><a href=\"http:\/\/truyentranhtuan\.com\/(.*?)\/\"(.*?)<\/a><\/span>/", $homepage, $match);

		foreach ($match[1] as $key => $value) {
			$item = array();
			$item["truyen_code"] = $value;
			$this->Truyen->insertTruyen($item);
		}
		// print_r($match[1]);
	}

	public function UpdateAllTruyen()
	{
		$list_truyen = array();
		set_time_limit(3600);
		$truyens = $this->Truyen->GetAll();
		$i = 0;
		foreach ($truyens as $key => $value) {
			$list_truyen[] = $this->getThongTinTruyen($value);
			$i++;
			echo $i."-";
		}
		$this->Truyen->updateBatchTruyen($list_truyen);
	}

	public function updatechuong()
	{
		set_time_limit(3600);
		$this->Chuong->updateChuong();
	}

	public function updateTruyenEveryDay()
	{
		$list_truyen = array();
		set_time_limit(3600);
		$detail = @file_get_contents('http://truyentranhtuan.com/');
		$detail = str_replace(array("\n", "\r", "\t", "  "), "", $detail);
		preg_match_all("/<span class=\"manga easy-tooltip\"><a href=\"(.*?)\">(.*?)<\/a><\/span>/", $detail, $match);
		// print_r($match[1]);
		foreach ($match[1] as $key => $value) {
			$detail = @file_get_contents($value);
			$arr    = explode("/", $value);
			$truyen = $this->Truyen->getTruyenByCode($arr[count($arr)-2]);
			$list_truyen[] = $this->getThongTinTruyen($truyen);
			// print_r($truyen);
		}

		// print_r($list_truyen);exit();
		$this->Truyen->updateBatchTruyen($list_truyen);
		echo json_encode($list_truyen);
	}

	function getThongTinTruyen($truyen, $num = null){
		$detail = @file_get_contents('http://truyentranhtuan.com/'.$truyen->truyen_code);
		$detail = str_replace(array("\n", "\r", "\t", "  "), "", $detail);

		if(!$truyen->truyen_image)
		{
			preg_match_all("/<div class=\"manga-cover\"><img width=\"200\" src=\"(.*?)\"/", $detail, $match);
			$arr = explode("/", $match[1][0]);
			$url = dirname(__FILE__)."/../../../public/img/".$arr[count($arr)-1];
			$puf = file_get_contents($match[1][0]);
			file_put_contents($url, $puf);
			$truyen->truyen_image = $arr[count($arr)-1];
		}
		
		if(!$truyen->truyen_ten){
			preg_match_all("/<div><h1 itemprop=\"name\">(.*?)<\/h1>/", $detail, $match);
			$truyen->truyen_ten = $match[1][0];
		}

		if(!$truyen->truyen_tac_gia){
			preg_match_all("/<spanitemprop=\"author\" itemscope itemtype(.*?)<span itemprop=\"name\">(.*?)<\/span><\/span>/", $detail, $match);
			$truyen->truyen_tac_gia = $match[2][0];
		}

		if(!$truyen->truyen_the_loai){
			preg_match_all("/<a href=\"\/danh-sach-truyen\/the-loai\/(.*?)\" itemprop=\"genre\">(.*?)<\/a>/", $detail, $match);
			$theloai = "";
			for($i = 0 ; $i < count($match[1]); $i++)
			{
				$item = array();
				$item["theloai_code"] = $match[1][$i];
				$item["theloai_ten"]  = $match[2][$i];
				$this->Theloai->insertTheLoai($item);
				$theloai .= $match[1][$i].",";
			}
			$truyen->truyen_the_loai = $theloai;
		}

		if(!$truyen->truyen_tinh_trang){
			preg_match_all("/<a href=\"\/danh-sach-truyen\/trang-thai\/(.*?)\">(.*?)<\/a>/", $detail, $match);
			$truyen->truyen_tinh_trang = $match[1][0];
		}
		
		if(!$truyen->truyen_noi_dung){
			preg_match_all("/<div id=\"manga-summary\"><p>(.*?)<\/p>(.*?)<\/div>/", $detail, $match);
			$truyen->truyen_noi_dung = $match[1][0];
		}

		preg_match_all("/<span class=\"chapter-name\"><a href=\"http:\/\/truyentranhtuan\.com\/(.*?)\/\">(.*?)<\/a><\/span>(.*?)<span class=\"group-name\">(.*?)<\/span>(.*?)<span class=\"date-name\">(.*?)<\/span>/", $detail, $match);
		$arr                          = explode("-", $match[1][0]);
		$truyen->truyen_chuong_moi     = $arr[count($arr)-1];
		$truyen->truyen_tong_so_chuong = $arr[count($arr)-1];
		$arr                          = explode(".", $match[6][0]);
		$truyen->truyen_date_update    = $arr[2]."-".$arr[1]."-".$arr[0];
		
		if(!$num){
			$num = ($truyen->truyen_ten)?3:count($match[6]);
		}else{
			$num = count($match[6]);
		}

		for($i = 0; $i < $num; $i++)
		{
			$arr                        = explode(".", $match[6][$i]);
			$item                       = array();
			$item["truyen_code"]        = $truyen->truyen_code;
			$item["chuong_code"]        = $match[1][$i];
			$item["chuong_date_update"] = $arr[2]."-".$arr[1]."-".$arr[0];
			$item["chuong_nhom_dich"]   = $match[4][$i];
			$arr                        = explode("-", $match[1][$i]);
			$item["chuong_num"]         = (is_numeric($arr[count($arr)-2]))?$arr[count($arr)-2].".".$arr[count($arr)-1]:$arr[count($arr)-1];
			if($item["chuong_code"]){
				$this->Chuong->insertChuong($item);	
			}
		}
		return $truyen;
		// $this->Truyen->updateTruyen($truyen);
		// print_r($truyen);exit();
	}

	public function updateTruyenByCodeTruyen($code_truyen)
	{
		$truyen = $this->Truyen->getTruyenByCode($code_truyen);
		if($truyen)
		{
			$result = $this->getThongTinTruyen($truyen, 1);
			$this->Truyen->updateTruyen($result);
			echo json_encode($result);
		}else{
			echo "no action!!";
		}
	}

	public function test()
	{
		echo "test";
	}
}

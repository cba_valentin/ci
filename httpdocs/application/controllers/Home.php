<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model("User");
	}

	public function index()
	{
		redirect("truyen/");
	}

	public function login()
	{
		if($this->input->post("user_name") && $this->input->post("user_password"))
		{
			$data["user_name"] = $this->input->post("user_name");
			$data["user_password"] = $this->input->post("user_password");
			$resutl = $this->User->checkUser($data);
			if($resutl)
			{
				$this->session->user = $resutl;
				if($resutl->user_admin_stage == 1)
				{
					redirect("admin/home/");	
				}else
				{
					redirect("truyen/home/");	
				}
			}
		}

		$this->load->view("login.html");
	}

	public function register()
	{
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		$this->config->set_item('language', 'vietnamese');
		$config = array(
				array(
					'field' => 'user[user_display_name]',
					'label' => 'Họ và Tên',
					'rules' => 'trim|required|max_length[20]'
				),
				array(
					'field' => 'user[user_email]',
					'label' => 'Email',
					'rules' => 'trim|required|max_length[50]|valid_email'
				),
				array(
					'field' => 'user[user_name]',
					'label' => 'Tên đăng nhập',
					'rules' => 'trim|required|max_length[20]|min_length[6]|callback_user_name_check_unique'
				),
				array(
					'field' => 'user[user_password]',
					'label' => 'Mật khẩu',
					'rules' => 'trim|required|min_length[6]'
				),
				array(
					'field' => 'user[user_password_confirm]',
					'label' => 'Xác nhận mật khẩu',
					'rules' => 'trim|required|min_length[6]|matches[user[user_password]]'
				),
			);
		// $this->form_validation->set_message();
		$this->form_validation->set_rules($config);
		if($this->form_validation->run())
		{
			$user = $this->input->post("user");
			// print_r($user)
			$user['user_password'] = $this->encrypt->encode($user['user_password']);
			unset($user['user_password_confirm']);
			$result = $this->User->insertUser($user);
			$user['id'] = $result;
			$this->session->user = (object)$user;
			redirect("user/home");
		}

		$this->load->view("register.html");
	}

	public function user_name_check_unique($user_name)
	{
		if($this->User->getUserByUserName($user_name))
		{
			$this->form_validation->set_message("user_name_check_unique", "Tài khoản đã tồn tại");
			return false;
		}
		return true;
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect("home/login");
	}

	public function contact()
	{
		$this->load->library("email");
		$this->email->from("user");
		$this->email->to("xnvnhthi@xn--vnhthi-kta6n495y.vn");
		$this->email->subject($this->input->post("tieude"));
		$this->email->message($this->input->post("noidung"));
		$this->email->send();
		// echo $this->email->print_debugger();
		echo "ok";
	}

}

<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['required']           = "Bạn phải nhập %s.";
$lang['isset']              = "Bạn phải nhập giá trị cho %s.";
$lang['valid_email']        = "Bạn phải nhập email đúng định dạng cho %s.";
$lang['valid_emails']       = "Bạn phải nhập các email đúng định dạng cho %s.";
$lang['valid_url']          = "Bạn phải nhập URL đúng định dạng cho %s.";
$lang['valid_ip']           = "Bạn phải nhập IP đúng định dạng cho %s.";
$lang['min_length']         = "Trường %s phải có ít nhất %s ký tự.";
$lang['max_length']         = "Trường %s không được vượt quá %s ký tự.";
$lang['exact_length']       = "Trường %s chỉ được phép có %s ký tự.";
$lang['alpha']              = "Trường %s chỉ được phép điền các ký tự chữ cái.";
$lang['alpha_numeric']      = "Trường %s chỉ được phép điền các ký tự chữ và số.";
$lang['alpha_dash']         = "Trường %s chỉ được phép điền các ký tự chữ và số, dấu gạch dưới và dấu gạch ngang.";
$lang['numeric']            = "Trường %s chỉ được phép điền các ký tự số.";
$lang['is_numeric']         = "Trường %s chỉ được phép điền các ký tự số.";
$lang['integer']            = "Trường %s chỉ được phép điền số nguyên.";
$lang['regex_match']        = "Trường %s không đúng định dạng.";
$lang['matches']            = "Trường %s không khớp với trường %s.";
$lang['is_unique']          = "Trường %s phải có giá trị là duy nhất.";
$lang['is_natural']         = "Trường %s chỉ được phép điền số tự nhiên.";
$lang['is_natural_no_zero'] = "Trường %s chỉ được phép điền số dương lớn hơn 0.";
$lang['decimal']            = "Trường %s chỉ được phép điền số thập phân.";
$lang['less_than']          = "Trường %s chỉ được phép điền số nhỏ hơn %s.";
$lang['greater_than']       = "Trường %s chỉ được phép điền số lớn hơn %s.";

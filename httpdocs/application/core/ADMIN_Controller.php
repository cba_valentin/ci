<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ADMIN_Controller extends CI_Controller {

	function __Construct()
	{
		parent::__Construct();
		if(!$this->session->user && uri_string() != "home/login" && $this->session->user->user_admin_stage != 1)
		{
			redirect("home/login");
		}
	}
}
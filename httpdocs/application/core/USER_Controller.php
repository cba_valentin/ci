<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class USER_Controller extends CI_Controller {

	function __Construct()
	{
		parent::__Construct();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		if(!$this->session->user && uri_string() != "home/login")
		{
			redirect("home/login");
		}
	}
}
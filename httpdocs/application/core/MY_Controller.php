<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __Construct()
	{
		parent::__Construct();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		define("APPID", "1545037959110689");
	}
}